<?php
/**
 * @file
 * A description of what your module does.
 */

/**
 * Implements hook_help().
 */
function clash_clan_help($path, $arg) {
  switch ($path) {
    // Main module help for the block module.
    case 'admin/help#block':
      return '<p>' . t('Blocks are boxes of content rendered into an area, or region, of a web page. The default theme Bartik, for example, implements the regions "Sidebar first", "Sidebar second", "Featured", "Content", "Header", "Footer", etc., and a block may appear in any one of these areas. The <a href="@blocks">blocks administration page</a> provides a drag-and-drop interface for assigning a block to a region, and for controlling the order of blocks within regions.', array('@blocks' => url('admin/structure/block'))) . '</p>';

    // Help for the Admin Page.
    case 'admin/config/services/clash':
      return '<p>' . t('This page will allow you to configure your api key') . '</p>';
  }
}

/**
 * Implements hook_permission().
 */
function clash_clan_permission() {
  return array(
    'administer clash of clans' => array(
      'title' => 'Configure Clash of Clans',
      'description' => 'Allow access to Configure Clash of Clans',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function clash_clan_menu() {
  $items = array();
  $items['admin/config/services/clash'] = array(
    'title' => 'Configure Clash of Clans',
    'description' => 'Different Configuration options for Clash of Clans module',
    'page callback' => 'drupal_get_form',
    'access arguments' => array('administer clash of clans'),
    'page arguments' => array('clash_clan_settings_form'),
  );
  $items['clan'] = array(
    'title' => 'My Clan Page',
    'description' => 'Its my clan baby',
    'access arguments' => array('access content'),
    'page callback' => 'clash_clan_my_clan',
    'menu_name' => 'main-menu',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['clan/members'] = array(
    'title' => 'Clan Members',
    'description' => 'Members of the Clan',
    'access arguments' => array('access content'),
    'page callback' => 'clash_clan_members',
    'menu_name' => 'main-menu',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_settings_form().
 */
function clash_clan_settings_form($form, &$form_state) {
  $form = array();
  $form['overview'] = array(
    '#markup' => t('Configure Clash of Clans Module'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['api_key'] = array(
    '#title' => t('Clash of Clans API Key'),
    '#description' => t('The API key for Supercel to access their servers'),
    '#type' => 'textarea',
    '#default_value' => variable_get('api_key'),
    '#required' => TRUE,
  );
  $form['clan_tag'] = array(
    '#title' => t('Your Clan Tag'),
    '#description' => t('The Tag of your clan, do not add the #.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('clan_tag'),
    '#field_prefix' => '#',
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function clash_clan_settings_validate($form, &$form_state) {
  $api_key_validate = $form_state['values']['api_key'];
  $clan_tag_validate = $form_state['values']['clan_tag'];
  if (strpos($clan_tag_validate, '#')) {
    form_set_error('clan_tag', t('Clan Tag must start with a #.'));
  }
}

/**
 * Custom Page to display main clan data.
 *
 * @return string
 *   Returns rendered content.
 */
function clash_clan_my_clan() {
  // Cache the data from external api.
  $clan_data = &drupal_static(__FUNCTION__);
  if (!isset($clan_data)) {
    // Get the cached data.
    $cache = cache_get('clan_data', 'cache_clash_clan');
    // Check to see if we got cache and if the expire time is still good.
    if ($cache && !(time() > $cache->expire)) {
      $clan_data = $cache->data;
    }
    else {
      // Options array for the request.
      $options = array(
        'method' => 'GET',
        'headers' => array(
          'Accept' => 'application/json',
          'authorization ' => 'Bearer ' . variable_get('api_key'),
        ),
      );
      $my_request = drupal_http_request('https://api.clashofclans.com/v1/clans/%' . variable_get('clan_tag') . '', $options);
      $clan_data = drupal_json_decode($my_request->data);
      // Check to see if page lifetime is used, if so use that off set.
      $clash_clan_cache_life = variable_get('cache_lifetime', CACHE_TEMPORARY);
      if ($clash_clan_cache_life > CACHE_TEMPORARY) {
        $clash_clan_cache_life += REQUEST_TIME;
      }
      // Cache the data.
      cache_set('clan_data', $clan_data, 'cache_clash_clan', $clash_clan_cache_life);
    }
  }
  return clash_clan_clan_table($clan_data);
}

/**
 * Implements theme_table().
 *
 * Simple parser to use drupal table array building.
 */
function clash_clan_clan_table($clan_data) {
  $header = array(
    'Clan Badge',
    'Clan Name',
    'Clan Level',
    'War Wins',
    'Members',
  );
  $rows = array();
  $rows[] = array(
    array(
      'data' => '<img src=' . $clan_data['badgeUrls']['small'] . '>',
      'style' => 'text-align: center;',
    ),
    l($clan_data['name'], 'clan/members'),
    $clan_data['clanLevel'],
    $clan_data['warWins'],
    $clan_data['members'],
  );
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'class' => array(
        'table',
        'table-striped',
        'table-hover',
      ),
    ),
    'sticky' => FALSE,
  ));
}

/**
 * Page display for all member.
 *
 * @return string
 *   Rendered html table of all the members.
 */
function clash_clan_members() {
  $member_list = &drupal_static(__FUNCTION__);
  if (!isset($member_list)) {
    // Get the cached data.
    $cache = cache_get('clan_member_list', 'cache_clash_clan');
    // Check to see if we got the data and if data is still good.
    if ($cache && !(time() > $cache->expire)) {
      $member_list = $cache->data;
    }
    else {
      // Options array for the request.
      $options = array(
        'method' => 'GET',
        'headers' => array(
          'Accept' => 'application/json',
          'authorization ' => 'Bearer ' . variable_get('api_key'),
        ),
      );
      // Get data from remote api server.
      $my_request = drupal_http_request('https://api.clashofclans.com/v1/clans/%' . variable_get('clan_tag') . '/members', $options);
      // Json decode the data into a drupal array for processing.
      $member_list = drupal_json_decode($my_request->data);
      // Check to see if page lifetime is used, if so use that off set.
      $clash_clan_cache_life = variable_get('cache_lifetime', CACHE_TEMPORARY);
      if ($clash_clan_cache_life > CACHE_TEMPORARY) {
        $clash_clan_cache_life += REQUEST_TIME;
      }
      // Cache the results.
      cache_set('clan_member_list', $member_list, 'cache_clash_clan', $clash_clan_cache_life);
    }
  }

  return clash_clan_members_table($member_list);
}

/**
 * Implements theme_table().
 */
function clash_clan_members_table($clan_data) {
  $header = array(
    'Name',
    'Role',
    'Level',
    'Rank',
    'League',
  );

  $rows = array();
  foreach ($clan_data['items'] as $member) {
    $rows[] = array(
      $member['name'],
      $member['role'],
      $member['expLevel'],
      $member['clanRank'],
      theme('image', array(
        'path' => $member['league']['iconUrls']['tiny'],
        'alt' => 'Member League',
        'title' => NULL,
        'attributes' => NULL,
        'getsize' => TRUE,
      )
      ) . ' ' . $member['league']['name'],
    );
  }
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'class' => array(
        'table',
        'table-striped',
        'table-hover',
      ),
    ),
    'sticky' => FALSE,
  )
  );
}

/**
 * Implements hook_flush_caches().
 */
function clash_clan_flush_caches() {
  return array('cache_clash_clan');
}
